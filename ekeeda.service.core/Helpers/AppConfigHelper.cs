﻿using ekeeda.service.core.Models.Enum;

namespace ekeeda.service.core.Helpers
{
    public class AppConfigHelper
    {
        public AppConfigHelper(EnvironmentModeEnum environmentMode, string? baseUrl, string? whatsappNumber, string whatsappApiKey)
        {
            EnvironmentMode = environmentMode;
            BaseUrl = baseUrl;
            WhatsappNumber = whatsappNumber;
            WhatsappApiKey = whatsappApiKey;
        }

        public EnvironmentModeEnum EnvironmentMode { get; private set; }

        public string? BaseUrl { get; private set; }

        public string? WhatsappNumber { get; private set; }
        public string WhatsappApiKey { get; private set; }
    }
}
