﻿using System.Data;
using System.Data.SqlClient;

namespace ekeeda.service.core.Helpers
{
    public interface IDBHelper
    {
        object DefaultDate(DateTime? registrationExpiryDate);
        void ExecuteNonQuery(string spName, SqlParameter[] sqlParameters);
        int ExecuteNonQuery(string spName, SqlParameter[] sqlParameters, CommandType commandType);
        object ExecuteScalar(string spName, SqlParameter[] sqlParameters);
        object ExecuteScalar(string spName, SqlParameter[] sqlParameters, CommandType commandType);
        DataSet GetDataSet(string spName, SqlParameter[] sqlParameters);
        DataTable GetDataTable(string spName, SqlParameter[] sqlParameters);
        DataTable GetDataTable(string spName, SqlParameter[] sqlParameters, CommandType commandType);
    }
}