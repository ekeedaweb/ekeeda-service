﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ekeeda.service.core.Helpers
{
    public static class CommonHelper
    {

        public static bool ParseBoolean(object value, bool defaultValue = false)
        {
            return Convert.IsDBNull(value) ? defaultValue : Convert.ToBoolean(value);
        }
        public static bool? ParseBooleanNull(object value)
        {
            if (Convert.IsDBNull(value))
            {
                return null;
            }

            return Convert.ToBoolean(value);
        }
        public static string ParseString(object value, string defaultValue = "")
        {
            return Convert.IsDBNull(value) ? defaultValue : Convert.ToString(value).Trim();
        }
        public static int ParseInt32(object value, int defaultValue = 0)
        {
            return Convert.IsDBNull(value) ? defaultValue : Convert.ToInt32(value);
        }
        public static int? ParseInt32Null(object value)
        {
            if (Convert.IsDBNull(value))
            {
                return null;
            }

            return Convert.ToInt32(value);
        }
        public static long ParseInt64(object value, long defaultValue = 0)
        {
            return Convert.IsDBNull(value) ? defaultValue : Convert.ToInt64(value);
        }
        public static decimal ParseDecimal(object value, decimal defaultValue = 0)
        {
            return Convert.IsDBNull(value) ? defaultValue : Convert.ToDecimal(value);
        }
        public static double ParseDouble(object value, double defaultValue = 0)
        {
            return Convert.IsDBNull(value) ? defaultValue : Convert.ToDouble(value);
        }

        public static DateTime ParseDateTime(object value)
        {
            return Convert.IsDBNull(value) ? DateTime.Now : Convert.ToDateTime(value);
        }
        public static string ParseDateTimeAsString(object value, string format = "dd/MM/yyyy")
        {
            if (Convert.IsDBNull(value))
            {
                return string.Empty;
            }
            else
            {
                return Convert.ToDateTime(value).ToString(format);
            }
        }
        public static DateTime? ParseDateTimeNull(object value)
        {
            return Convert.IsDBNull(value) || string.IsNullOrEmpty(Convert.ToString(value)) ? (DateTime?)null : Convert.ToDateTime(value);
        }
        public static byte[] ParseByte(object value)
        {
            return Convert.IsDBNull(value) ? (byte[])value : null;
        }
        public static T ParseEnumByValue<T>(int v)
        {
            return (T)Enum.ToObject(typeof(T), v);
        }


        public static T ParseEnumByText<T>(string v)
        {
            return (T)Enum.Parse(typeof(T), v);
        }

        public static Guid ParseGuid(object value)
        {
            return Convert.IsDBNull(value) ? Guid.Empty : Guid.Parse(value.ToString());
        }

        public static List<string> ParseCSV(object value)
        {
            return Convert.IsDBNull(value) || string.IsNullOrEmpty(value.ToString()) ? new List<string>() : value.ToString().Split(',').ToList();
        }

        public static List<int> ParseIntCSV(object value)
        {
            return Convert.IsDBNull(value) || string.IsNullOrEmpty(value.ToString()) ? new List<int>() : value.ToString().Split(',').Select(int.Parse).ToList();
        }


        public static string DefaultString(string value)
        {
            return string.IsNullOrEmpty(value) ? string.Empty : value;
        }
        public static string getTimeAgo(DateTime yourDate)
        {
            var ts = new TimeSpan(DateTime.UtcNow.Ticks - yourDate.Ticks);
            double delta = Math.Abs(ts.TotalSeconds);

            if (delta < 1 * 60)
                return ts.Seconds == 1 ? "one second ago" : ts.Seconds + " seconds ago";

            if (delta < 2 * 60)
                return "a minute ago";

            if (delta < 45 * 60)
                return ts.Minutes + " minutes ago";

            if (delta < 90 * 60)
                return "an hour ago";

            if (delta < 24 * 3600)
                return ts.Hours + " hours ago";

            if (delta < 48 * 3600)
                return "yesterday";

            if (delta < 30 * 86400)
                return ts.Days + " days ago";

            if (delta < 12 * 2592000)
            {
                int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                return months <= 1 ? "one month ago" : months + " months ago";
            }
            else
            {
                int years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
                return years <= 1 ? "one year ago" : years + " years ago";
            }
        }
    }
}
