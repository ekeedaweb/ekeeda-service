﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ekeeda.service.core.Helpers
{
    public class DBHelper : IDBHelper
    {
        private readonly ILogger<DBHelper> logger;

        private string connectionString = "";

        private readonly string strSQL = "SQL";

        private readonly string strSystem = "System";

        private readonly string strDetailError = "Detail Error";

        public DBHelper(IConfiguration configuration,
            ILogger<DBHelper> logger)
        {
            this.logger = logger;
            this.connectionString = configuration.GetConnectionString("DefaultConnection");
             this.logger.LogInformation($"connectionString: {connectionString}");
        }

        #region Public Methods

        #region DB Execuation Methods

        public void ExecuteNonQuery(string spName, SqlParameter[] sqlParameters)
        {
            SqlConnection sqlConnection = new SqlConnection(this.connectionString);
            sqlConnection.Open();
            string strMessage = string.Empty;
            string strErrorMessage = string.Empty;

            SqlCommand sqlCommand = new SqlCommand(spName, sqlConnection);

            try
            {
                sqlCommand.CommandTimeout = 180;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                if (sqlParameters != null && sqlParameters.Length > 0)
                {
                    sqlCommand.Parameters.AddRange(sqlParameters);
                }

                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException exSQL)
            {
                HandleSqlException(exSQL, ref strMessage, ref strErrorMessage);
                throw new Exception(strMessage, exSQL);
            }
            catch (Exception ex)
            {
                HandleException(ex, ref strMessage, ref strErrorMessage);
                throw new Exception(strMessage, ex);
            }
            finally
            {
                sqlCommand.Dispose();
                sqlConnection.Close();
            }
        }
        public int ExecuteNonQuery(string spName, SqlParameter[] sqlParameters, CommandType commandType)
        {
            SqlConnection sqlConnection = new SqlConnection(this.connectionString);
            sqlConnection.Open();
            string strMessage = string.Empty;
            string strErrorMessage = string.Empty;

            SqlCommand sqlCommand = new SqlCommand(spName, sqlConnection);

            try
            {
                sqlCommand.CommandTimeout = 180;
                sqlCommand.CommandType = commandType;

                if (sqlParameters != null && sqlParameters.Length > 0)
                {
                    sqlCommand.Parameters.AddRange(sqlParameters);
                }

                return sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException exSQL)
            {
                HandleSqlException(exSQL, ref strMessage, ref strErrorMessage);
                throw new Exception(strMessage, exSQL);
            }
            catch (Exception ex)
            {
                HandleException(ex, ref strMessage, ref strErrorMessage);
                throw new Exception(strMessage, ex);
            }
            finally
            {
                sqlCommand.Dispose();
                sqlConnection.Close();
            }
        }

        public object ExecuteScalar(string spName, SqlParameter[] sqlParameters)
        {
            DataTable dataTable = GetDataTable(spName, sqlParameters);

            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return dataTable.Rows[0][0];
            }

            return null;
        }

        public object ExecuteScalar(string spName, SqlParameter[] sqlParameters, CommandType commandType)
        {
            DataTable dataTable = GetDataTable(spName, sqlParameters, commandType);
            return dataTable.Rows[0][0];
        }

        public DataSet GetDataSet(string spName, SqlParameter[] sqlParameters)
        {
            DataSet ds = new DataSet();
            SqlConnection sqlConnection = new SqlConnection(this.connectionString);
            sqlConnection.Open();
            string strMessage = string.Empty;
            string strErrorMessage = string.Empty;

            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(spName, sqlConnection);

            try
            {
                sqlDataAdapter.SelectCommand.CommandTimeout = 180;
                sqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (sqlParameters != null && sqlParameters.Length > 0)
                {
                    sqlDataAdapter.SelectCommand.Parameters.AddRange(sqlParameters);
                }

                sqlDataAdapter.Fill(ds);

                return ds;
            }
            catch (SqlException exSQL)
            {
                HandleSqlException(exSQL, ref strMessage, ref strErrorMessage);
                throw new Exception(strMessage, exSQL);
            }
            catch (Exception ex)
            {
                HandleException(ex, ref strMessage, ref strErrorMessage);
                throw new Exception(strMessage, ex);
            }
            finally
            {
                sqlDataAdapter.Dispose();
                ds.Dispose();
                sqlConnection.Close();
            }
        }

        public DataTable GetDataTable(string spName, SqlParameter[] sqlParameters)
        {
            DataSet ds = new DataSet();
            SqlConnection sqlConnection = new SqlConnection(this.connectionString);
            sqlConnection.Open();

            string strMessage = string.Empty;
            string strErrorMessage = string.Empty;

            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(spName, sqlConnection);

            try
            {
                sqlDataAdapter.SelectCommand.CommandTimeout = 180;
                sqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (sqlParameters != null && sqlParameters.Length > 0)
                {
                    sqlDataAdapter.SelectCommand.Parameters.AddRange(sqlParameters);
                }

                sqlDataAdapter.Fill(ds);

                if (ds != null && ds.Tables.Count > 0)
                {
                    return ds.Tables[0];
                }

                return null;
            }
            catch (SqlException exSQL)
            {
                HandleSqlException(exSQL, ref strMessage, ref strErrorMessage);
                throw new Exception(strMessage, exSQL);
            }
            catch (Exception ex)
            {
                HandleException(ex, ref strMessage, ref strErrorMessage);
                throw new Exception(strMessage, ex);
            }
            finally
            {
                sqlDataAdapter.Dispose();
                ds.Dispose();
                sqlConnection.Close();
            }
        }

        public DataTable GetDataTable(string spName, SqlParameter[] sqlParameters, CommandType commandType)
        {
            DataSet ds = new DataSet();
            SqlConnection sqlConnection = new SqlConnection(this.connectionString);
            sqlConnection.Open();

            string strMessage = string.Empty;
            string strErrorMessage = string.Empty;

            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(spName, sqlConnection);

            try
            {
                sqlDataAdapter.SelectCommand.CommandTimeout = 180;
                sqlDataAdapter.SelectCommand.CommandType = commandType;

                if (sqlParameters != null && sqlParameters.Length > 0)
                {
                    sqlDataAdapter.SelectCommand.Parameters.AddRange(sqlParameters);
                }

                sqlDataAdapter.Fill(ds);

                if (ds != null && ds.Tables.Count > 0)
                {
                    return ds.Tables[0];
                }

                return null;
            }
            catch (SqlException exSQL)
            {
                HandleSqlException(exSQL, ref strMessage, ref strErrorMessage);
                throw new Exception(strMessage, exSQL);
            }
            catch (Exception ex)
            {
                HandleException(ex, ref strMessage, ref strErrorMessage);
                throw new Exception(strMessage, ex);
            }
            finally
            {
                sqlDataAdapter.Dispose();
                ds.Dispose();
                sqlConnection.Close();
            }
        }

        public object DefaultDate(DateTime? registrationExpiryDate)
        {
            return registrationExpiryDate.HasValue ? registrationExpiryDate : null;
        }

        #endregion

        #region DB Value Casting Methods

        #endregion

        #endregion

        #region Helper Methods

        private void HandleSqlException(SqlException exSQL, ref string strMessage, ref string strErrorMessage)
        {
            strErrorMessage = strSQL + "|" + exSQL.Class.ToString() + "|" + exSQL.LineNumber.ToString() + "|" + exSQL.Message + "|" + exSQL.Procedure;
            strMessage = exSQL.Class == 16 ? strDetailError : exSQL.Message;
        }

        private void HandleException(Exception ex, ref string strMessage, ref string strErrorMessage)
        {
            strErrorMessage = strSystem + "|" + ex.Message;
            strMessage = "Oops.  We're sorry but there seems to be an issue.";
        }

        #endregion
    }
}
