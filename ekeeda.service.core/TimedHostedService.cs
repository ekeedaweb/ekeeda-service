﻿using ekeeda.service.core.Helpers;
using ekeeda.service.core.Models;
using ekeeda.service.core.Repositories;
using System.Text;

namespace ekeeda.service.core
{
    internal class TimedHostedService : IHostedService, IDisposable
    {
        private readonly IFollowupNotificationRepository followupNotificationRepository;
        private readonly AppConfigHelper appConfigHelper;
        private readonly ILogger<TimedHostedService> _logger;
        private Timer? _timer = null;
        private Timer? _timer2 = null;
        private bool isWhatappNotificationProcess = false;
        private bool isFollowupNotificationProcess = false;

        public TimedHostedService(IFollowupNotificationRepository followupNotificationRepository,
            AppConfigHelper appConfigHelper,
            ILogger<TimedHostedService> logger)
        {
            this.followupNotificationRepository = followupNotificationRepository;
            this.appConfigHelper = appConfigHelper;
            _logger = logger;
        }

        public void Dispose()
        {
            _timer?.Dispose();
            _timer2?.Dispose();
        }

        protected void WhatsAppStartTimer(TimeSpan scheduledRunTime, TimeSpan timeBetweenEachRun)
        {
            // Initialize timer
            double current = DateTime.Now.TimeOfDay.TotalMilliseconds;
            double scheduledTime = scheduledRunTime.TotalMilliseconds;
            double intervalPeriod = timeBetweenEachRun.TotalMilliseconds;
            // calculates the first execution of the method, either its today at the scheduled time or tomorrow (if scheduled time has already occurred today)
            double firstExecution = current > scheduledTime ? intervalPeriod - (current - scheduledTime) : scheduledTime - current;

            // create callback - this is the method that is called on every interval
            TimerCallback callback = new TimerCallback(ProcessSendWhatsappNotification);

            // create timer
            this._timer2 = new Timer(callback, null, Convert.ToInt32(firstExecution), Convert.ToInt32(intervalPeriod));
        }
        protected void StartTimer(TimeSpan scheduledRunTime, TimeSpan timeBetweenEachRun)
        {
            // Initialize timer
            double current = DateTime.Now.TimeOfDay.TotalMilliseconds;
            double scheduledTime = scheduledRunTime.TotalMilliseconds;
            double intervalPeriod = timeBetweenEachRun.TotalMilliseconds;
            // calculates the first execution of the method, either its today at the scheduled time or tomorrow (if scheduled time has already occurred today)
            double firstExecution = current > scheduledTime ? intervalPeriod - (current - scheduledTime) : scheduledTime - current;

            // create callback - this is the method that is called on every interval
            TimerCallback callback = new TimerCallback(ProcessSendFollowupNotification);

            // create timer
            this._timer = new Timer(callback, null, Convert.ToInt32(firstExecution), Convert.ToInt32(intervalPeriod));

        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Hosted Service running.");

            if (isFollowupNotificationProcess == false)
            {
                _timer = new Timer(ProcessSendFollowupNotification, null,
                    TimeSpan.Zero,
                    TimeSpan.FromDays(1));
                //StartTimer(new TimeSpan(6, 0, 0), new TimeSpan(24, 0, 0));
            }

            if (isWhatappNotificationProcess == false)
            {
                // Pass in the time you want to start and the interval
                _timer2 = new Timer(ProcessSendWhatsappNotification, null,
                    TimeSpan.Zero,
                    TimeSpan.FromMinutes(5));
                //WhatsAppStartTimer(new TimeSpan(0, 5, 0), new TimeSpan(0, 5, 0));
            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Hosted Service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }


        private void ProcessSendFollowupNotification(object? state)
        {
            isFollowupNotificationProcess = true;
            _logger.LogInformation(string.Format("{0}:{1} {2}", "ProcessSendFollowupNotification", "Service is recall at", DateTime.Now));
            FollowupNotificationUpdateModel obj = new FollowupNotificationUpdateModel();
            try
            {
                this.followupNotificationRepository.GetFollowupNotificationModel();

                var data = this.followupNotificationRepository.FollowupNotificationList();

                if (data.Count > 0)
                {
                    _logger.LogInformation(string.Format("{0}:Notification detected: {1}", "ProcessSendFollowupNotification", data.Count));
                    foreach (var notificationModel in data)
                    {
                        try
                        {

                            obj.NotificationId = notificationModel.NotificationId;

                            var url = appConfigHelper.BaseUrl.Replace("{Key}", GenerateRatingUserKey(notificationModel.UserId, notificationModel.FeedBackId, notificationModel.CategoryTypeId));
                            var duration = CommonHelper.getTimeAgo(notificationModel.OrderDate);

                            notificationModel.Content = notificationModel.Content.Replace("##Name##", notificationModel.StudentName);

                            notificationModel.Content = notificationModel.Content.Replace("##Duration##", duration);
                            var shortUrl = followupNotificationRepository.GetShortUrl(url);
                            notificationModel.Content = notificationModel.Content.Replace("##Url##", shortUrl);


                            _logger.LogInformation(string.Format("Notification Content: {0}", notificationModel.Content));

                            if (notificationModel.NotificationType == 3)
                            {
                                notificationModel.Content = notificationModel.Content.Replace("##Url##", url);
                                EmailModel email = new EmailModel();
                                email.EmailTo = notificationModel.Email;
                                email.Subject = notificationModel.Title;
                                email.Body = notificationModel.Content;
                                followupNotificationRepository.SendMail(email);
                                obj.Response = "Send Email Successfully";
                            }
                            else
                            {
                                SMSModel sms = new SMSModel();
                                sms.SMSMobileNumber = notificationModel.Mobile;
                                sms.SMSBody = notificationModel.Content;
                                followupNotificationRepository.SMSSendAsync(sms);
                                obj.Response = "Send SMS Successfully";
                            }
                            obj.Status = 1;
                            this.followupNotificationRepository.UpdateNotificationStatus(obj);
                        }
                        catch (Exception ex1)
                        {
                            _logger.LogInformation(string.Format("Notification Content{0}:{1} ", notificationModel.NotificationId, ex1.Message));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Response = "Sending Fail";
                obj.Status = 2;
                this.followupNotificationRepository.UpdateNotificationStatus(obj);
                throw ex;
            }
            _logger.LogInformation(string.Format("{0}:{1} {2}", "ProcessSendFollowupNotification", "Service is stopped at", DateTime.Now));
            isFollowupNotificationProcess = false;
        }

        public void ProcessSendWhatsappNotification(object? state)
        {
            isWhatappNotificationProcess = true;
            _logger.LogInformation(string.Format("{0}:{1} {2}", "ProcessSendWhatsappNotification", "Service is recall at", DateTime.Now));
            FollowupNotificationUpdateModel obj = new FollowupNotificationUpdateModel();
            try
            {
                this.followupNotificationRepository.GetFollowupNotificationModel();

                var data = this.followupNotificationRepository.FollowupNotificationList();

                if (data.Count > 0)
                {
                    _logger.LogInformation(string.Format("{0}:Notification detected: {1}", "ProcessSendWhatsappNotification", data.Count));
                    foreach (var notificationModel in data)
                    {
                        try
                        {

                            obj.NotificationId = notificationModel.NotificationId;
                            notificationModel.Content = notificationModel.Content.Replace("##Name##", notificationModel.StudentName);

                            _logger.LogInformation(string.Format("Notification Content: {0}", notificationModel.Content));

                            if (notificationModel.NotificationType == 3)
                            {
                                WhatsappModel model = new WhatsappModel();
                                if (!string.IsNullOrWhiteSpace(notificationModel.SalesPersonWhatappNumber))
                                {
                                    model.FromWhatappNumber = notificationModel.SalesPersonWhatappNumber;
                                }

                                if (!string.IsNullOrWhiteSpace(notificationModel.WhatsappApiKey))
                                {
                                    model.WhatsappApiKey = notificationModel.WhatsappApiKey;
                                }

                                model.MobileNumber = notificationModel.Mobile;
                                model.Content = notificationModel.Content;

                                var res = followupNotificationRepository.SendWhatsappMsgAsyc(model, model.FromWhatappNumber, model.WhatsappApiKey);
                                if (res.Result == true)
                                {
                                    obj.Response = "Message Succesfully.";                                 
                                }
                                else
                                {
                                    obj.Response = "Failed To Send Message!";                                  
                                }
                                obj.Status = 1;

                                this.followupNotificationRepository.UpdateNotificationStatus(obj);
                            }
                        }
                        catch (Exception ex1)
                        {
                            _logger.LogInformation(string.Format("Notification Content{0}:{1} ", notificationModel.NotificationId, ex1.Message));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Response = "Sending Fail";
                obj.Status = 2;
                this.followupNotificationRepository.UpdateNotificationStatus(obj);
                throw ex;
            }
            _logger.LogInformation(string.Format("{0}:{1} {2}", "ProcessSendWhatsappNotification", "Service is stopped at", DateTime.Now));
            isWhatappNotificationProcess = false;
        }

        public string GenerateRatingUserKey(long userId, long? feedbackId, int? categoryTypeId)
        {
            string keyFormat = string.Format("{0}-{1}-{2}-{3}", userId, feedbackId, categoryTypeId, DateTime.Now.Ticks);
            byte[] data = ASCIIEncoding.ASCII.GetBytes(keyFormat);
            return Convert.ToBase64String(data);
        }
    }
}
