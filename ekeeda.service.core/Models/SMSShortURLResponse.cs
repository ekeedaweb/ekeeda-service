﻿namespace ekeeda.service.core.Models
{
    public class SMSShortURLResponse
    {
        public long id { get; set; }
        public string status { get; set; }
        public string message { get; set; }
        public string token { get; set; }
        public string txtly { get; set; }
    }
}
