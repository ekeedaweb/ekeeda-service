﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ekeeda.service.core.Models.Enum
{
    public enum EnvironmentModeEnum
    {
        Dev = 0,

        QA = 1,

        Production = 2
    }
}
