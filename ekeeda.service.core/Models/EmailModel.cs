﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ekeeda.service.core.Models
{
    public class EmailModel
    {
        public string EmailFrom { get; set; }


        public string EmailTo { get; set; }


        public string CC { get; set; }


        public string BCC { get; set; }


        public string Body { get; set; }


        public string Subject { get; set; }
    }
}
