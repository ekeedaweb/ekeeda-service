﻿namespace ekeeda.service.core.Models
{
    public class WhatsappModel
    {
        public string MessageUrl { get; set; }
        public string MessageTitle { get; set; }
        public string Content { get; set; }
        public string MobileNumber { get; set; }
        public string FromWhatappNumber { get; set; }
        public string WhatsappApiKey { get; set; }
    }   
}
