﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ekeeda.service.core.Models
{
    public class SMSModel
    {
        public string SMSSenderName { get; set; }

        public string SMSEntityId { get; set; }

        public string SMSTemplateId { get; set; }

        public string SMSMobileNumber { get; set; }

        public string SMSBody { get; set; }

        public int Route { get; set; }

        public string SMSNewTemplateId { get; set; }
    }
}
