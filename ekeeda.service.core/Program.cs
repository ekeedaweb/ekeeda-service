using ekeeda.service.core;
using ekeeda.service.core.Helpers;
using ekeeda.service.core.Models.Enum;
using ekeeda.service.core.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Extensions.Logging;
using System;
using System.Reflection;

internal class Program
{
    private static readonly LoggerProviderCollection Providers = new LoggerProviderCollection();

    public static IConfiguration Configuration { get; } = new ConfigurationBuilder()
          .SetBasePath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location))
          .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
          .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true)
          .AddEnvironmentVariables()
          .Build();

    
    private static async Task Main(string[] args)
    {
        Log.Logger = new LoggerConfiguration()
                    .ReadFrom.Configuration(Configuration)
                    .WriteTo.Providers(Providers)
                    .Enrich.FromLogContext()
                    //.WriteTo.Seq("http://localhost:5341/")
                    .CreateLogger();

        try
        {
            IHost host = Host.CreateDefaultBuilder(args)
                        .ConfigureServices(services =>
                        {
                            //services.AddHostedService<Worker>();
                            services.AddHostedService<TimedHostedService>();
                            //Log.Error("Configuration", Configuration["BaseUrl"]);
                            services.AddSingleton(new AppConfigHelper((EnvironmentModeEnum)Enum.Parse(typeof(EnvironmentModeEnum), Configuration["EnvironmentMode"]),
                                Configuration["BaseUrl"],
                                Configuration["WhatsappNumber"],
                                Configuration["whatsappApiKey"]));
                            //services.AddSingleton(new DBHelper(Configuration));

                            services.AddSingleton<IDBHelper, DBHelper>();

                            ////services.AddScoped<IDBHelper>(options =>
                            ////{
                            ////    return new DBHelper(Configuration, Log.Logger);
                            ////});
                            services.AddSingleton<IFollowupNotificationRepository, FollowupNotificationRepository>();

                        })
                        .UseSerilog(providers: Providers)
                        .UseWindowsService()
                        .Build();

            await host.RunAsync();
        }
        catch (Exception ex)
        {
            Log.Fatal(ex, "Host terminated unexpectedly");
        }
        finally
        {
            Log.CloseAndFlush();
        }
    }
}