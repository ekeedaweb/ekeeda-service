﻿
using ekeeda.service.core.Models;

namespace ekeeda.service.core.Repositories
{
    public interface IFollowupNotificationRepository
    {
        void GetFollowupNotificationModel();
        void UpdateNotificationStatus(FollowupNotificationUpdateModel notificationUpdateModel);
        List<FollowupNotificationModel> FollowupNotificationList();
        Task<bool> SendMail(EmailModel emailModel);
        Task<bool> SMSSendAsync(SMSModel smsModel);
        Task<bool> SendWhatsappMsgAsyc(WhatsappModel model, string whatsappNumber, string whatsappapikey);
        string GetShortUrl(string url);

    }
}
