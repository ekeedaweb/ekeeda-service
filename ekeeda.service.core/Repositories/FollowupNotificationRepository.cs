﻿using ekeeda.service.core.Helpers;
using ekeeda.service.core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ekeeda.service.core.Repositories
{
    public class FollowupNotificationRepository : IFollowupNotificationRepository
    {
        #region Variable Declaration
        private readonly IDBHelper dbHelper;

        private readonly ILogger<FollowupNotificationRepository> logger;

        private readonly string SMSUrl = "https://alerts.kaleyra.com/api/v4/";

        private readonly string SMSApiKey = "A50c1b0213de44fc2e0011fff4e14f45b";

        private readonly string SMSSender = "EKEEDA";

        private readonly string SMSEnityId = "11019240000027895";

        private readonly string prefix = "SPN_Service_FollowupNotification_";

        private readonly string SMSNewTemplateId = "1107164819079156387";

        private readonly string WhatsappUser = "ekeeda";
        private readonly string WhatsappPassword = "12345";
        private readonly string WhatsappNumber = "919324088810";
        private readonly string WhatsappApiKey = "a9787d6a5e1edc428b69";
        //private readonly string MessageUrl = "http://37.59.76.45/api/mt/SendSMS";
        private readonly string MessageUrl = "http://whatsapp.visionhlt.com/api/mt/SendSMS";

        #endregion

        #region Constructor

        public FollowupNotificationRepository(IDBHelper dBHelper,
            ILogger<FollowupNotificationRepository> logger)
        {
            this.dbHelper = dBHelper;
            this.logger = logger;
        }



        #endregion

        #region Public Methods        


        public void GetFollowupNotificationModel()
        {
            try
            {
                dbHelper.ExecuteNonQuery(prefix + "Load", null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FollowupNotificationModel> FollowupNotificationList()
        {
            List<FollowupNotificationModel> notificationLoadModel = new List<FollowupNotificationModel>();

            try
            {
                var dataset = dbHelper.GetDataSet(prefix + "List", null);

                if (dataset != null && dataset.Tables.Count > 0)
                {
                    if (dataset.Tables[0].Rows != null && dataset.Tables[0].Rows.Count > 0)
                    {
                        FollowupNotificationModel notification;
                        foreach (DataRow row in dataset.Tables[0].Rows)
                        {
                            notification = new FollowupNotificationModel();
                            notification.NotificationId = CommonHelper.ParseInt32(row["NotificationId"]);
                            notification.UserId = CommonHelper.ParseInt32(row["UserId"]);
                            notification.Email = CommonHelper.ParseString(row["Email"]);
                            notification.Mobile = CommonHelper.ParseString(row["Mobile"]);
                            notification.Title = CommonHelper.ParseString(row["Title"]);
                            notification.Content = CommonHelper.ParseString(row["Content"]);
                            notification.Status = CommonHelper.ParseInt32(row["Status"]);
                            notification.FeedBackId = CommonHelper.ParseInt32(row["FeedBackId"]);
                            notification.CreatedOn = CommonHelper.ParseDateTime(row["CreatedOn"]);
                            notification.NotificationFor = CommonHelper.ParseInt32(row["NotificationFor"]);
                            notification.NotificationType = CommonHelper.ParseInt32(row["NotificationType"]);
                            notification.CategoryTypeId = CommonHelper.ParseInt32(row["CategoryTypeId"]);

                            notification.StudentName = CommonHelper.ParseString(row["StudentName"]);
                            notification.OrderDate = CommonHelper.ParseDateTime(row["OrderDate"]);
                            notification.SalesPersonWhatappNumber = CommonHelper.ParseString(row["SalesPersonWhatappNumber"]);
                            notification.WhatsappApiKey = CommonHelper.ParseString(row["WhatsappApiKey"]);
                            notificationLoadModel.Add(notification);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return notificationLoadModel;
        }

        public void UpdateNotificationStatus(FollowupNotificationUpdateModel notificationUpdateModel)
        {
            try
            {
                List<SqlParameter> sqlParamsList = new List<SqlParameter>();
                sqlParamsList.Add(new SqlParameter("NotificationId", notificationUpdateModel.NotificationId));
                sqlParamsList.Add(new SqlParameter("status", notificationUpdateModel.Status));
                sqlParamsList.Add(new SqlParameter("Response", notificationUpdateModel.Response));
                dbHelper.ExecuteNonQuery(prefix + "Update_Status", sqlParamsList.ToArray());
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<bool> SendMail(EmailModel emailModel)
        {
            MailMessage msg = new MailMessage();
            try
            {
                SmtpClient client = new SmtpClient("email-smtp.ap-south-1.amazonaws.com", 587);
                client.Credentials = new NetworkCredential("AKIAXOFQCXHPAUHVO37N", "BGx5eoz9EXqyWRWa9DlcLMzi9DxAtA1EYQGWby3ieLcH");
                // Enable SSL encryption
                client.EnableSsl = true;

                if (emailModel.EmailTo.Contains(","))
                {
                    foreach (var emailToArr in emailModel.EmailTo.Split(','))
                    {
                        msg.To.Add(emailToArr);
                    }
                }
                else
                {
                    msg.To.Add(emailModel.EmailTo);
                }

                if (!string.IsNullOrEmpty(emailModel.CC))
                {
                    if (emailModel.CC.Contains(","))
                    {
                        foreach (var ccArr in emailModel.CC.Split(','))
                        {
                            msg.CC.Add(ccArr);
                        }
                    }
                    else
                    {
                        msg.CC.Add(emailModel.CC);
                    }
                }

                if (!string.IsNullOrEmpty(emailModel.BCC))
                {
                    if (emailModel.BCC.Contains(","))
                    {
                        foreach (var bccArr in emailModel.BCC.Split(','))
                        {
                            msg.Bcc.Add(bccArr);
                        }
                    }
                    else
                    {
                        msg.Bcc.Add(emailModel.BCC);
                    }
                }

                if (!string.IsNullOrEmpty("care@ekeedamail.in"))
                {
                    msg.From = new MailAddress("care@ekeedamail.in", "Ekeeda");
                }
                else if (!string.IsNullOrEmpty(emailModel.EmailFrom))
                {
                    msg.From = new MailAddress(emailModel.EmailFrom, "Ekeeda");
                }

                msg.Subject = emailModel.Subject;
                msg.Body = emailModel.Body;
                msg.IsBodyHtml = true;
                msg.Priority = MailPriority.High;
                object userstate = msg;
                await client.SendMailAsync(msg);
                return true;
            }

            catch (SmtpFailedRecipientException ex1)
            {
                Console.WriteLine(ex1.Message.ToString());
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetShortUrl(string url)
        {
            //Your message to send, Add URL encoding here.
            string message = HttpUtility.UrlEncode(url);
            try
            {
                string strUrl = "https://api-alerts.kaleyra.com/v5/?method=txtly.create&api_key={0}&url={1}&token=&title=&advanced=0";

                strUrl = String.Format(strUrl, SMSApiKey, message);

                WebRequest request = HttpWebRequest.Create(strUrl);
                var response = request.GetResponse();
                Stream s = (Stream)response.GetResponseStream();
                StreamReader readStream = new StreamReader(s);
                string dataString = readStream.ReadToEnd();
                response.Close();
                s.Close();
                readStream.Close();

                var Obj = Newtonsoft.Json.JsonConvert.DeserializeObject<SMSShortURLResponse>(dataString);

                return Obj.txtly;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> SMSSendAsync(SMSModel smsModel)
        {
            //Your message to send, Add URL encoding here.
            string message = HttpUtility.UrlEncode(smsModel.SMSBody);
            try
            {
                smsModel.SMSSenderName = SMSSender;
                smsModel.SMSEntityId = SMSEnityId;

                string strUrl = this.SMSUrl + "?method=sms&api_key={0}&message={1}&to={2}&sender={3}";

                strUrl = String.Format(strUrl, SMSApiKey, message, smsModel.SMSMobileNumber, smsModel.SMSSenderName);

                if (!string.IsNullOrEmpty(smsModel.SMSEntityId))
                {
                    strUrl += "&entity_id=" + smsModel.SMSEntityId;
                }
                if (!string.IsNullOrEmpty(SMSNewTemplateId))
                {
                    strUrl += "&template_id=" + SMSNewTemplateId;
                }

                WebRequest request = HttpWebRequest.Create(strUrl);
                var response = await request.GetResponseAsync();
                Stream s = (Stream)response.GetResponseStream();
                StreamReader readStream = new StreamReader(s);
                string dataString = readStream.ReadToEnd();
                response.Close();
                s.Close();
                readStream.Close();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> SendWhatsappMsgAsyc(WhatsappModel model, string whatsappNumber,string whatsappapikey)
        {
            string message = HttpUtility.UrlEncode(model.Content);
            try
            {
                var WhatsappUser = "";
                if (whatsappapikey == "d0609f8227096598addf")
                {
                    WhatsappUser = "ekeeda1";
                }
                else {
                    WhatsappUser = "ekeeda1";
                }
                var mobile = model.MobileNumber;
                if (mobile.Length <= 10) {
                    mobile = $"91{mobile}";
                }

                string url = this.MessageUrl + "?user={0}&password={1}&Apiauthkey={2}&mobile={3}&messageText={4}&fileurl={5}";
                url = string.Format(url, WhatsappUser, WhatsappPassword, whatsappapikey, mobile, message,"");
                logger.LogInformation($"Mobile:{mobile} Time:{DateTime.Now}");
                logger.LogInformation($"WhatsApp Service API Url:{url}");

                using var httpClient = new HttpClient();
                var request = new HttpRequestMessage(HttpMethod.Post, url);
                var response = httpClient.Send(request);
                using var reader = new StreamReader(response.Content.ReadAsStream());
                var responseBody = reader.ReadToEnd();
                logger.LogInformation($"WhatsApp Service API Response:{responseBody}");

                //WebRequest request = HttpWebRequest.Create(url);
                //var response = await request.GetResponseAsync();
                //Stream s = (Stream)response.GetResponseStream();
                //StreamReader readStream = new StreamReader(s);
                //string dataString = readStream.ReadToEnd();
                //response.Close();
                //s.Close();
                //readStream.Close();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return await Task.FromResult(true);
                }

                return await Task.FromResult(false);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
    }
}
