﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ekeeda.Windows.Services.Enumerations
{
    public enum EnvironmentModeEnum
    {
        Dev,

        QA,

        Production
    }
}
