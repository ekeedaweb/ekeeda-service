﻿
namespace Ekeeda.Windows.Services
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.serviceProcessInstaller2 = new System.ServiceProcess.ServiceProcessInstaller();
            this.followupServices1 = new System.ServiceProcess.ServiceInstaller();
            this.whatsAppServices1 = new System.ServiceProcess.ServiceInstaller();
            // 
            // followupServices1
            // 
            //this.followupServices1.ExitCode = 0;
            this.followupServices1.ServiceName = "FollowUpService";
            // 
            // whatsAppServices1
            // 
            //this.whatsAppServices1.ExitCode = 0;
            this.whatsAppServices1.ServiceName = "WhatsAppServices";

            //this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            //this.serviceProcessInstaller2,
            //this.followupServices1});

            //this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            //this.serviceProcessInstaller2,
            //this.whatsAppServices1});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller2;
        private System.ServiceProcess.ServiceInstaller followupServices1;
        private System.ServiceProcess.ServiceInstaller whatsAppServices1;
        //private FollowupServices followupServices1;
        //private WhatsAppServices whatsAppServices1;
    }
}