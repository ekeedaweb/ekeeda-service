﻿using Ekeeda.Windows.Services.Models;
using myfirst.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace Ekeeda.Windows.Services.Repositories
{
    public class FollowupNotificationRepository : IFollowupNotificationRepository
    {
        #region Variable Declaration

        private readonly string SMSUrl = "https://alerts.kaleyra.com/api/v4/";

        private readonly string SMSApiKey = "A50c1b0213de44fc2e0011fff4e14f45b";

        private readonly string SMSSender = "EKEEDA";

        private readonly string SMSEnityId = "11019240000027895";

        private readonly string prefix = "SPN_Service_FollowupNotification_";

        private readonly string SMSNewTemplateId = "1107164819079156387";

        private readonly string WhatsappUser = "Ekeeda";
        private readonly string WhatsappPassword = "12345";
        private readonly string WhatsappApiKey = "a9787d6a5e1edc428b69";
        private readonly string MessageUrl = "http://37.59.76.45/api/mt/SendSMS?";

        #endregion

        #region Public Methods        


        public void GetFollowupNotificationModel()
        {
            try
            {
                DBHelper.ExecuteNonQuery(prefix + "Load", null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FollowupNotificationModel> FollowupNotificationList()
        {
            List<FollowupNotificationModel> notificationLoadModel = new List<FollowupNotificationModel>();

            try
            {
                var dataset = DBHelper.GetDataSet(prefix + "List", null);

                if (dataset != null && dataset.Tables.Count > 0)
                {
                    if (dataset.Tables[0].Rows != null && dataset.Tables[0].Rows.Count > 0)
                    {
                        FollowupNotificationModel notification;
                        foreach (DataRow row in dataset.Tables[0].Rows)
                        {
                            notification = new FollowupNotificationModel();
                            notification.NotificationId = DBHelper.ParseInt32(row["NotificationId"]);
                            notification.UserId = DBHelper.ParseInt32(row["UserId"]);
                            notification.Email = DBHelper.ParseString(row["Email"]);
                            notification.Mobile = DBHelper.ParseString(row["Mobile"]);
                            notification.Title = DBHelper.ParseString(row["Title"]);
                            notification.Content = DBHelper.ParseString(row["Content"]);
                            notification.Status = DBHelper.ParseInt32(row["Status"]);
                            notification.FeedBackId = DBHelper.ParseInt32(row["FeedBackId"]);
                            notification.CreatedOn = DBHelper.ParseDateTime(row["CreatedOn"]);
                            notification.NotificationFor = DBHelper.ParseInt32(row["NotificationFor"]);
                            notification.NotificationType = DBHelper.ParseInt32(row["NotificationType"]);
                            notification.CategoryTypeId = DBHelper.ParseInt32(row["CategoryTypeId"]);

                            notification.StudentName = DBHelper.ParseString(row["StudentName"]);
                            notification.OrderDate = DBHelper.ParseDateTime(row["OrderDate"]);
                            notification.SalesPersonWhatappNumber = DBHelper.ParseString(row["SalesPersonWhatappNumber"]);
                            notificationLoadModel.Add(notification);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return notificationLoadModel;
        }

        public void UpdateNotificationStatus(FollowupNotificationUpdateModel notificationUpdateModel)
        {
            try
            {
                List<SqlParameter> sqlParamsList = new List<SqlParameter>();
                sqlParamsList.Add(new SqlParameter("NotificationId", notificationUpdateModel.NotificationId));
                sqlParamsList.Add(new SqlParameter("status", notificationUpdateModel.Status));
                sqlParamsList.Add(new SqlParameter("Response", notificationUpdateModel.Response));
                DBHelper.ExecuteNonQuery(prefix + "Update_Status", sqlParamsList.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> SendMail(EmailModel emailModel)
        {
            MailMessage msg = new MailMessage();
            try
            {
                SmtpClient client = new SmtpClient("email-smtp.ap-south-1.amazonaws.com", 587);
                client.Credentials = new NetworkCredential("AKIAXOFQCXHPAUHVO37N", "BGx5eoz9EXqyWRWa9DlcLMzi9DxAtA1EYQGWby3ieLcH");
                // Enable SSL encryption
                client.EnableSsl = true;

                if (emailModel.EmailTo.Contains(","))
                {
                    foreach (var emailToArr in emailModel.EmailTo.Split(','))
                    {
                        msg.To.Add(emailToArr);
                    }
                }
                else
                {
                    msg.To.Add(emailModel.EmailTo);
                }

                if (!string.IsNullOrEmpty(emailModel.CC))
                {
                    if (emailModel.CC.Contains(","))
                    {
                        foreach (var ccArr in emailModel.CC.Split(','))
                        {
                            msg.CC.Add(ccArr);
                        }
                    }
                    else
                    {
                        msg.CC.Add(emailModel.CC);
                    }
                }

                if (!string.IsNullOrEmpty(emailModel.BCC))
                {
                    if (emailModel.BCC.Contains(","))
                    {
                        foreach (var bccArr in emailModel.BCC.Split(','))
                        {
                            msg.Bcc.Add(bccArr);
                        }
                    }
                    else
                    {
                        msg.Bcc.Add(emailModel.BCC);
                    }
                }

                if (!string.IsNullOrEmpty("care@ekeedamail.in"))
                {
                    msg.From = new MailAddress("care@ekeedamail.in", "Ekeeda");
                }
                else if (!string.IsNullOrEmpty(emailModel.EmailFrom))
                {
                    msg.From = new MailAddress(emailModel.EmailFrom, "Ekeeda");
                }

                msg.Subject = emailModel.Subject;
                msg.Body = emailModel.Body;
                msg.IsBodyHtml = true;
                msg.Priority = MailPriority.High;
                object userstate = msg;
                await client.SendMailAsync(msg);
                return true;
            }

            catch (SmtpFailedRecipientException ex1)
            {
                Console.WriteLine(ex1.Message.ToString());
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetShortUrl(string url)
        {
            //Your message to send, Add URL encoding here.
            string message = HttpUtility.UrlEncode(url);
            try
            {
                string strUrl = "https://api-alerts.kaleyra.com/v5/?method=txtly.create&api_key={0}&url={1}&token=&title=&advanced=0";

                strUrl = String.Format(strUrl, SMSApiKey, message);

                WebRequest request = HttpWebRequest.Create(strUrl);
                var response = request.GetResponse();
                Stream s = (Stream)response.GetResponseStream();
                StreamReader readStream = new StreamReader(s);
                string dataString = readStream.ReadToEnd();
                response.Close();
                s.Close();
                readStream.Close();
                var serializer = new JavaScriptSerializer();

                var Obj = serializer.Deserialize<SMSShortURLResponse>(dataString);

                return Obj.txtly;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> SMSSendAsync(SMSModel smsModel)
        {
            //Your message to send, Add URL encoding here.
            string message = HttpUtility.UrlEncode(smsModel.SMSBody);
            try
            {
                smsModel.SMSSenderName = SMSSender;
                smsModel.SMSEntityId = SMSEnityId;

                string strUrl = this.SMSUrl + "?method=sms&api_key={0}&message={1}&to={2}&sender={3}";

                strUrl = String.Format(strUrl, SMSApiKey, message, smsModel.SMSMobileNumber, smsModel.SMSSenderName);

                if (!string.IsNullOrEmpty(smsModel.SMSEntityId))
                {
                    strUrl += "&entity_id=" + smsModel.SMSEntityId;
                }
                if (!string.IsNullOrEmpty(SMSNewTemplateId))
                {
                    strUrl += "&template_id=" + SMSNewTemplateId;
                }

                WebRequest request = HttpWebRequest.Create(strUrl);
                var response = await request.GetResponseAsync();
                Stream s = (Stream)response.GetResponseStream();
                StreamReader readStream = new StreamReader(s);
                string dataString = readStream.ReadToEnd();
                response.Close();
                s.Close();
                readStream.Close();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> SendMessageAsyc(WhatsappModel model)
        {
            string message = HttpUtility.UrlEncode(model.Content);
            try
            {
                string url = this.MessageUrl + "?method=sms&user={0}&password={1}&api_key={2}";
                url = string.Format(url, WhatsappApiKey, message, model.MobileNumber);
                WebRequest request = HttpWebRequest.Create(url);
                var response = await request.GetResponseAsync();
                Stream s = (Stream)response.GetResponseStream();
                StreamReader readStream = new StreamReader(s);
                string dataString = readStream.ReadToEnd();
                response.Close();
                s.Close();
                readStream.Close();
                return true;

            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
    }
}
