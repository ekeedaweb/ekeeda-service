﻿using Ekeeda.Windows.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ekeeda.Windows.Services.Repositories
{
    public interface IFollowupNotificationRepository
    {
        void GetFollowupNotificationModel();
        void UpdateNotificationStatus(FollowupNotificationUpdateModel notificationUpdateModel);
        List<FollowupNotificationModel> FollowupNotificationList();
        Task<bool> SendMail(EmailModel emailModel);
        Task<bool> SMSSendAsync(SMSModel smsModel);
        Task<bool> SendMessageAsyc(WhatsappModel model);
        string GetShortUrl(string url);
    }
}
