﻿using Ekeeda.Windows.Services.Enumerations;
using Ekeeda.Windows.Services.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ekeeda.Windows.Services.Helpers
{
    public static class AppConfigHelper
    {
        public static EnvironmentModeEnum EnvironmentMode
        {
            get
            {
                return (EnvironmentModeEnum)Enum.Parse(typeof(EnvironmentModeEnum), Settings.Default.EnvironmentMode);
            }
        }
        public static string BaseUrl
        {
            get
            {
                return Settings.Default.BaseUrl;
            }
        }

        public static string ConnectionString
        {
            get
            {
                switch (EnvironmentMode)
                {
                    case EnvironmentModeEnum.Dev:
                        return Settings.Default.EkeedaConnection_Dev;
                    case EnvironmentModeEnum.QA:
                        return Settings.Default.EkeedaConnection_QA;
                    case EnvironmentModeEnum.Production:
                        return Settings.Default.EkeedaConnection_Production;
                }

                return string.Empty;
            }
        
        
        }

        public static string WhatsappNumber
        {
            get
            {
                return Settings.Default.WhatsappNumber;
            }
        }
    }
}
