﻿namespace myfirst.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    public static class DBHelper
    {
        #region  Public Variables

        public static string connectionString = ConfigurationManager.AppSettings["EkeedaConnection"];

      
        private static string strSQL = "SQL";

      
        private static string strSystem = "System";

     
        private static string strDetailError = "Detail Error";

        #endregion

        #region Public Methods

        #region DB Execuation Methods

    
        public static void ExecuteNonQuery(string spName, SqlParameter[] sqlParameters)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            string strMessage = string.Empty;
            string strErrorMessage = string.Empty;

            SqlCommand sqlCommand = new SqlCommand(spName, sqlConnection);

            try
            {
                sqlCommand.CommandTimeout = 180;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                if (sqlParameters != null && sqlParameters.Length > 0)
                {
                    sqlCommand.Parameters.AddRange(sqlParameters);
                }

                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException exSQL)
            {
                HandleSqlException(exSQL, ref strMessage, ref strErrorMessage);
                throw new Exception(strMessage, exSQL);
            }
            catch (Exception ex)
            {
                HandleException(ex, ref strMessage, ref strErrorMessage);
                throw new Exception(strMessage, ex);
            }
            finally
            {
                sqlCommand.Dispose();
                sqlConnection.Close();
            }
        }
        public static int ExecuteNonQuery(string spName, SqlParameter[] sqlParameters, CommandType commandType)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            string strMessage = string.Empty;
            string strErrorMessage = string.Empty;

            SqlCommand sqlCommand = new SqlCommand(spName, sqlConnection);

            try
            {
                sqlCommand.CommandTimeout = 180;
                sqlCommand.CommandType = commandType;

                if (sqlParameters != null && sqlParameters.Length > 0)
                {
                    sqlCommand.Parameters.AddRange(sqlParameters);
                }

                return sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException exSQL)
            {
                HandleSqlException(exSQL, ref strMessage, ref strErrorMessage);
                throw new Exception(strMessage, exSQL);
            }
            catch (Exception ex)
            {
                HandleException(ex, ref strMessage, ref strErrorMessage);
                throw new Exception(strMessage, ex);
            }
            finally
            {
                sqlCommand.Dispose();
                sqlConnection.Close();
            }
        }

        public static object ExecuteScalar(string spName, SqlParameter[] sqlParameters)
        {
            DataTable dataTable = GetDataTable(spName, sqlParameters);

            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return dataTable.Rows[0][0];
            }

            return null;
        }
     
        public static object ExecuteScalar(string spName, SqlParameter[] sqlParameters, CommandType commandType)
        {
            DataTable dataTable = GetDataTable(spName, sqlParameters, commandType);
            return dataTable.Rows[0][0];
        }

        public static DataSet GetDataSet(string spName, SqlParameter[] sqlParameters)
        {
            DataSet ds = new DataSet();
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            string strMessage = string.Empty;
            string strErrorMessage = string.Empty;

            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(spName, sqlConnection);

            try
            {
                sqlDataAdapter.SelectCommand.CommandTimeout = 180;
                sqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (sqlParameters != null && sqlParameters.Length > 0)
                {
                    sqlDataAdapter.SelectCommand.Parameters.AddRange(sqlParameters);
                }

                sqlDataAdapter.Fill(ds);

                return ds;
            }
            catch (SqlException exSQL)
            {
                HandleSqlException(exSQL, ref strMessage, ref strErrorMessage);
                throw new Exception(strMessage, exSQL);
            }
            catch (Exception ex)
            {
                HandleException(ex, ref strMessage, ref strErrorMessage);
                throw new Exception(strMessage, ex);
            }
            finally
            {
                sqlDataAdapter.Dispose();
                ds.Dispose();
                sqlConnection.Close();
            }
        }

        public static DataTable GetDataTable(string spName, SqlParameter[] sqlParameters)
        {
            DataSet ds = new DataSet();
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();

            string strMessage = string.Empty;
            string strErrorMessage = string.Empty;

            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(spName, sqlConnection);

            try
            {
                sqlDataAdapter.SelectCommand.CommandTimeout = 180;
                sqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (sqlParameters != null && sqlParameters.Length > 0)
                {
                    sqlDataAdapter.SelectCommand.Parameters.AddRange(sqlParameters);
                }

                sqlDataAdapter.Fill(ds);

                if (ds != null && ds.Tables.Count > 0)
                {
                    return ds.Tables[0];
                }

                return null;
            }
            catch (SqlException exSQL)
            {
                HandleSqlException(exSQL, ref strMessage, ref strErrorMessage);
                throw new Exception(strMessage, exSQL);
            }
            catch (Exception ex)
            {
                HandleException(ex, ref strMessage, ref strErrorMessage);
                throw new Exception(strMessage, ex);
            }
            finally
            {
                sqlDataAdapter.Dispose();
                ds.Dispose();
                sqlConnection.Close();
            }
        }

        public static DataTable GetDataTable(string spName, SqlParameter[] sqlParameters, CommandType commandType)
        {
            DataSet ds = new DataSet();
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();

            string strMessage = string.Empty;
            string strErrorMessage = string.Empty;

            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(spName, sqlConnection);

            try
            {
                sqlDataAdapter.SelectCommand.CommandTimeout = 180;
                sqlDataAdapter.SelectCommand.CommandType = commandType;

                if (sqlParameters != null && sqlParameters.Length > 0)
                {
                    sqlDataAdapter.SelectCommand.Parameters.AddRange(sqlParameters);
                }

                sqlDataAdapter.Fill(ds);

                if (ds != null && ds.Tables.Count > 0)
                {
                    return ds.Tables[0];
                }

                return null;
            }
            catch (SqlException exSQL)
            {
                HandleSqlException(exSQL, ref strMessage, ref strErrorMessage);
                throw new Exception(strMessage, exSQL);
            }
            catch (Exception ex)
            {
                HandleException(ex, ref strMessage, ref strErrorMessage);
                throw new Exception(strMessage, ex);
            }
            finally
            {
                sqlDataAdapter.Dispose();
                ds.Dispose();
                sqlConnection.Close();
            }
        }

        public static object DefaultDate(DateTime? registrationExpiryDate)
        {
            return registrationExpiryDate.HasValue ? registrationExpiryDate : null;
        }

        #endregion

        #region DB Value Casting Methods

        public static bool ParseBoolean(object value, bool defaultValue = false)
        {
            return Convert.IsDBNull(value) ? defaultValue : Convert.ToBoolean(value);
        }
        public static bool? ParseBooleanNull(object value)
        {
            if (Convert.IsDBNull(value))
            {
                return null;
            }

            return Convert.ToBoolean(value);
        }
        public static string ParseString(object value, string defaultValue = "")
        {
            return Convert.IsDBNull(value) ? defaultValue : Convert.ToString(value).Trim();
        }
        public static int ParseInt32(object value, int defaultValue = 0)
        {
            return Convert.IsDBNull(value) ? defaultValue : Convert.ToInt32(value);
        }
        public static int? ParseInt32Null(object value)
        {
            if (Convert.IsDBNull(value))
            {
                return null;
            }

            return Convert.ToInt32(value);
        }
        public static long ParseInt64(object value, long defaultValue = 0)
        {
            return Convert.IsDBNull(value) ? defaultValue : Convert.ToInt64(value);
        }
        public static decimal ParseDecimal(object value, decimal defaultValue = 0)
        {
            return Convert.IsDBNull(value) ? defaultValue : Convert.ToDecimal(value);
        }
        public static double ParseDouble(object value, double defaultValue = 0)
        {
            return Convert.IsDBNull(value) ? defaultValue : Convert.ToDouble(value);
        }

        public static DateTime ParseDateTime(object value)
        {
            return Convert.IsDBNull(value) ? DateTime.Now : Convert.ToDateTime(value);
        }
        public static string ParseDateTimeAsString(object value, string format = "dd/MM/yyyy")
        {
            if (Convert.IsDBNull(value))
            {
                return string.Empty;
            }
            else
            {
                return Convert.ToDateTime(value).ToString(format);
            }
        }
        public static DateTime? ParseDateTimeNull(object value)
        {
            return Convert.IsDBNull(value) || string.IsNullOrEmpty(Convert.ToString(value)) ? (DateTime?)null : Convert.ToDateTime(value);
        }
        public static byte[] ParseByte(object value)
        {
            return Convert.IsDBNull(value) ? (byte[])value : null;
        }
        public static T ParseEnumByValue<T>(int v)
        {
            return (T)Enum.ToObject(typeof(T), v);
        }

       
        public static T ParseEnumByText<T>(string v)
        {
            return (T)Enum.Parse(typeof(T), v);
        }
      
        public static Guid ParseGuid(object value)
        {
            return Convert.IsDBNull(value) ? Guid.Empty : Guid.Parse(value.ToString());
        }

        public static List<string> ParseCSV(object value)
        {
            return Convert.IsDBNull(value) || string.IsNullOrEmpty(value.ToString()) ? new List<string>() : value.ToString().Split(',').ToList();
        }

        public static List<int> ParseIntCSV(object value)
        {
            return Convert.IsDBNull(value) || string.IsNullOrEmpty(value.ToString()) ? new List<int>() : value.ToString().Split(',').Select(int.Parse).ToList();
        }

      
        public static string DefaultString(string value)
        {
            return string.IsNullOrEmpty(value) ? string.Empty : value;
        }

        #endregion

        #endregion

        #region Helper Methods

        private static void HandleSqlException(SqlException exSQL, ref string strMessage, ref string strErrorMessage)
        {
            strErrorMessage = strSQL + "|" + exSQL.Class.ToString() + "|" + exSQL.LineNumber.ToString() + "|" + exSQL.Message + "|" + exSQL.Procedure;
            strMessage = exSQL.Class == 16 ? strDetailError : exSQL.Message;
        }

        private static void HandleException(Exception ex, ref string strMessage, ref string strErrorMessage)
        {
            strErrorMessage = strSystem + "|" + ex.Message;
            strMessage = "Oops.  We're sorry but there seems to be an issue.";
        }
        public static string getTimeAgo(DateTime yourDate) {
            var ts = new TimeSpan(DateTime.UtcNow.Ticks - yourDate.Ticks);
            double delta = Math.Abs(ts.TotalSeconds);

            if (delta < 1 * 60)
                return ts.Seconds == 1 ? "one second ago" : ts.Seconds + " seconds ago";

            if (delta < 2 * 60)
                return "a minute ago";

            if (delta < 45 * 60)
                return ts.Minutes + " minutes ago";

            if (delta < 90 * 60)
                return "an hour ago";

            if (delta < 24 * 3600)
                return ts.Hours + " hours ago";

            if (delta < 48 * 3600)
                return "yesterday";

            if (delta < 30 * 86400)
                return ts.Days + " days ago";

            if (delta < 12 * 2592000)
            {
                int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                return months <= 1 ? "one month ago" : months + " months ago";
            }
            else
            {
                int years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
                return years <= 1 ? "one year ago" : years + " years ago";
            }
        }

        #endregion
    }
}
