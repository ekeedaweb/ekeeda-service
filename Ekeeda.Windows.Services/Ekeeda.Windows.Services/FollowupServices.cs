﻿using Ekeeda.Windows.Services.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Ekeeda.Windows.Services.Repositories;
using Ekeeda.Windows.Services.Models;
using System.Threading;
using Timer = System.Threading.Timer;
using myfirst.Helpers;

namespace Ekeeda.Windows.Services
{
    public partial class FollowupServices : ServiceBase
    {
        private IFollowupNotificationRepository followupNotificationRepository;

        private System.Threading.Timer notificationTimer = null;
        public FollowupServices()
        {
            InitializeComponent();
            if (AppConfigHelper.EnvironmentMode != Enumerations.EnvironmentModeEnum.Production)
            {
                this.ServiceName = string.Format("{0}.{1}", this.ServiceName, AppConfigHelper.EnvironmentMode.ToString());
            }
            this.followupNotificationRepository = new FollowupNotificationRepository();
        }

        protected override void OnStart(string[] args)
        {
            LogHelper.WriteToFile(string.Format("{0}:{1} {2}", this.ServiceName, "Service is started at", DateTime.Now));
            LogHelper.WriteToFile(string.Format("{0}", DBHelper.connectionString));
            // Pass in the time you want to start and the interval
            StartTimer(new TimeSpan(6, 0, 0), new TimeSpan(24, 0, 0));
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            LogHelper.WriteToFile(string.Format("{0}:{1} {2}", this.ServiceName, "Service is ended at", DateTime.Now));
        }


        protected void StartTimer(TimeSpan scheduledRunTime, TimeSpan timeBetweenEachRun)
        {
            // Initialize timer
            double current = DateTime.Now.TimeOfDay.TotalMilliseconds;
            double scheduledTime = scheduledRunTime.TotalMilliseconds;
            double intervalPeriod = timeBetweenEachRun.TotalMilliseconds;
            // calculates the first execution of the method, either its today at the scheduled time or tomorrow (if scheduled time has already occurred today)
            double firstExecution = current > scheduledTime ? intervalPeriod - (current - scheduledTime) : scheduledTime - current;

            // create callback - this is the method that is called on every interval
            TimerCallback callback = new TimerCallback(ProcessSendFollowupNotification);

            // create timer
            this.notificationTimer = new Timer(callback, null, Convert.ToInt32(firstExecution), Convert.ToInt32(intervalPeriod));

        }

        
        public void ProcessSendFollowupNotification(object state)
        {
            LogHelper.WriteToFile(string.Format("{0}:{1} {2}", this.ServiceName, "Service is recall at", DateTime.Now));
            FollowupNotificationUpdateModel obj = new FollowupNotificationUpdateModel();
            try
            {
                this.followupNotificationRepository.GetFollowupNotificationModel();

                var data = this.followupNotificationRepository.FollowupNotificationList();

                if (data.Count > 0)
                {
                    LogHelper.WriteToFile(string.Format("{0}:Notification detected: {1}", this.ServiceName, data.Count));
                    foreach (var notificationModel in data)
                    {
                        try
                        {

                            obj.NotificationId = notificationModel.NotificationId;

                            var url = AppConfigHelper.BaseUrl.Replace("{Key}", GenerateRatingUserKey(notificationModel.UserId, notificationModel.FeedBackId, notificationModel.CategoryTypeId));
                            var duration = DBHelper.getTimeAgo(notificationModel.OrderDate);

                            notificationModel.Content = notificationModel.Content.Replace("##Name##", notificationModel.StudentName);

                            notificationModel.Content = notificationModel.Content.Replace("##Duration##", duration);
                            var shortUrl = followupNotificationRepository.GetShortUrl(url);
                            notificationModel.Content = notificationModel.Content.Replace("##Url##", shortUrl);


                            LogHelper.WriteToFile(string.Format("Notification Content: {0}", notificationModel.Content));

                            if (notificationModel.NotificationType == 3)
                            {
                                notificationModel.Content = notificationModel.Content.Replace("##Url##", url);
                                EmailModel email = new EmailModel();
                                email.EmailTo = notificationModel.Email;
                                email.Subject = notificationModel.Title;
                                email.Body = notificationModel.Content;
                                followupNotificationRepository.SendMail(email);
                                obj.Response = "Send Email Successfully";
                            }
                            else
                            {
                                SMSModel sms = new SMSModel();
                                sms.SMSMobileNumber = notificationModel.Mobile;
                                sms.SMSBody = notificationModel.Content;
                                followupNotificationRepository.SMSSendAsync(sms);
                                obj.Response = "Send SMS Successfully";
                            }
                            obj.Status = 1;
                            this.followupNotificationRepository.UpdateNotificationStatus(obj);
                        }
                        catch (Exception ex1)
                        {
                            LogHelper.WriteToFile(string.Format("Notification Content{0}:{1} ", notificationModel.NotificationId, ex1.Message));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Response = "Sending Fail";
                obj.Status = 2;
                this.followupNotificationRepository.UpdateNotificationStatus(obj);
                throw ex;
            }
            LogHelper.WriteToFile(string.Format("{0}:{1} {2}", this.ServiceName, "Service is stopped at", DateTime.Now));
        }
        public static void SMSTest()
        {
            try
            {

                var followupNotificationRepository = new FollowupNotificationRepository();

                SMSModel sms = new SMSModel();
                sms.SMSMobileNumber = "7698585516";
                sms.SMSBody = "Ekeeda - Hi ##Name##, it is been ##Duration##! Hope you are exploring our courses. Do tell us about your fav class, positive points & challenges. Click - ##Url##";

                var url = AppConfigHelper.BaseUrl.Replace("{Key}", GenerateRatingUserKey(163315, 3745, 5));

                var shortUrl = followupNotificationRepository.GetShortUrl(url);

                var duration = DBHelper.getTimeAgo(DateTime.Now.AddDays(-2));

                sms.SMSBody = sms.SMSBody.Replace("##Name##", "Urvesh Joshi");

                sms.SMSBody = sms.SMSBody.Replace("##Duration##", duration);

                sms.SMSBody = sms.SMSBody.Replace("##Url##", shortUrl);

                followupNotificationRepository.SMSSendAsync(sms);

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static string GenerateRatingUserKey(long userId, long? feedbackId, int? categoryTypeId)
        {
            string keyFormat = string.Format("{0}-{1}-{2}-{3}", userId, feedbackId, categoryTypeId, DateTime.Now.Ticks);
            byte[] data = ASCIIEncoding.ASCII.GetBytes(keyFormat);
            return Convert.ToBase64String(data);
        }
    }
}
