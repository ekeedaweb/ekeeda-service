﻿using Ekeeda.Windows.Services.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Ekeeda.Windows.Services.Enumerations;
using myfirst.Helpers;

namespace Ekeeda.Windows.Services
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static void Main(string[] args)
        {
            string env = "QA";
            if (args.Length > 0)
            {
                var envValue = args.FirstOrDefault(arg => arg.StartsWith("env:"));
                if (!string.IsNullOrEmpty(envValue))
                {
                    env = envValue.Split(':')[1];
                }
            }

            switch (env)
            {
                case "Production":
                    Settings.Default.EnvironmentMode = EnvironmentModeEnum.Production.ToString();
                    DBHelper.connectionString = Settings.Default.EkeedaConnection_Production;
                    break;
                case "Dev":
                     Settings.Default.EnvironmentMode = EnvironmentModeEnum.Dev.ToString();
                    DBHelper.connectionString = Settings.Default.EkeedaConnection_Dev;
                    break;
                default:
                    Settings.Default.EnvironmentMode = EnvironmentModeEnum.QA.ToString();
                    DBHelper.connectionString = Settings.Default.EkeedaConnection_QA;
                    break;
            }
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new FollowupServices(),
                new WhatsAppServices()
            };
            
            ServiceBase.Run(ServicesToRun);
        }
    }
}
