﻿using Ekeeda.Windows.Services.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace Ekeeda.Windows.Services
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        #region Services

        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller1;
        private System.ServiceProcess.ServiceInstaller NotificationServiceInstaller;
        private System.ServiceProcess.ServiceInstaller WhatsappServiceInstaller;

        #endregion
        public ProjectInstaller()
        {
            InitializeComponent();

            this.serviceProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
            this.NotificationServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            this.WhatsappServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            
            // 
            // serviceProcessInstaller1
            // 
            
            this.serviceProcessInstaller1.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.serviceProcessInstaller1.Password = null;
            this.serviceProcessInstaller1.Username = null;

            // 
            // NotificationServiceInstaller
            // 
            this.NotificationServiceInstaller.Description = "Ekeeda Service for FollowupNotification";
            this.NotificationServiceInstaller.DisplayName = "Ekeeda.Service.FollowupNotification";
            this.NotificationServiceInstaller.ServiceName = "Ekeeda.Service.FollowupNotification";

            this.NotificationServiceInstaller.Description = "Ekeeda Service for FollowupNotification";
            this.NotificationServiceInstaller.DisplayName = "Ekeeda.Service.FollowupNotification";
            this.NotificationServiceInstaller.ServiceName = "Ekeeda.Service.FollowupNotification";

            this.WhatsappServiceInstaller.Description = "Ekeeda Service for WhatsappMessage";
            this.WhatsappServiceInstaller.DisplayName = "Ekeeda.Service.WhatsappMessage";
            this.WhatsappServiceInstaller.ServiceName = "Ekeeda.Service.WhatsappMessage";

            if (AppConfigHelper.EnvironmentMode != Enumerations.EnvironmentModeEnum.Production)
            {
                this.NotificationServiceInstaller.DisplayName = string.Format("{0}.{1}", this.NotificationServiceInstaller.DisplayName, AppConfigHelper.EnvironmentMode.ToString());
                this.NotificationServiceInstaller.ServiceName = string.Format("{0}.{1}", this.NotificationServiceInstaller.ServiceName, AppConfigHelper.EnvironmentMode.ToString());

                this.WhatsappServiceInstaller.DisplayName = string.Format("{0}.{1}", this.WhatsappServiceInstaller.DisplayName, AppConfigHelper.EnvironmentMode.ToString());
                this.WhatsappServiceInstaller.ServiceName = string.Format("{0}.{1}", this.WhatsappServiceInstaller.ServiceName, AppConfigHelper.EnvironmentMode.ToString());
            }

            this.NotificationServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            this.WhatsappServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.serviceProcessInstaller1,
            this.NotificationServiceInstaller,
            });


            this.Installers.AddRange(new System.Configuration.Install.Installer[]
            {
                this.serviceProcessInstaller1,
                this.WhatsappServiceInstaller
            });


        }
    }
}
