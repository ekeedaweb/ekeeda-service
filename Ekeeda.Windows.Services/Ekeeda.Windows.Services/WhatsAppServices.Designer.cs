﻿
namespace Ekeeda.Windows.Services
{
    partial class WhatsAppServices
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.projectInstaller1 = new Ekeeda.Windows.Services.ProjectInstaller();
            this.followupServices1 = new Ekeeda.Windows.Services.FollowupServices();
            // 
            // followupServices1
            // 
            this.followupServices1.ExitCode = 0;
            this.followupServices1.ServiceName = "Service1.QA";
            // 
            // WhatsAppServices
            // 
            this.ServiceName = "WhatsAppServices";

        }

        #endregion

        private ProjectInstaller projectInstaller1;
        private FollowupServices followupServices1;
    }
}
