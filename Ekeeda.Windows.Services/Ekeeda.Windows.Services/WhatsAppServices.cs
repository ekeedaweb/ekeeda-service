﻿using Ekeeda.Windows.Services.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Ekeeda.Windows.Services.Repositories;
using Ekeeda.Windows.Services.Models;
using System.Threading;
using Timer = System.Threading.Timer;
using myfirst.Helpers;

namespace Ekeeda.Windows.Services
{
    partial class WhatsAppServices : ServiceBase
    {
        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            LogHelper.WriteToFile(string.Format("{0}:{1} {2}", this.ServiceName, "Service is ended at", DateTime.Now));
        }


        private IFollowupNotificationRepository followupNotificationRepository;
        private Timer whatsappnotificationTimer = null;
    
        public WhatsAppServices()
        {
            InitializeComponent();
            if (AppConfigHelper.EnvironmentMode != Enumerations.EnvironmentModeEnum.Production)
            {
                this.ServiceName = string.Format("{0}.{1}", this.ServiceName, AppConfigHelper.EnvironmentMode.ToString());
            }
            this.followupNotificationRepository = new FollowupNotificationRepository();
        }

        protected override void OnStart(string[] args)
        {
            LogHelper.WriteToFile(string.Format("{0}:{1} {2}", this.ServiceName, "Service is started at", DateTime.Now));
            LogHelper.WriteToFile(string.Format("{0}", DBHelper.connectionString));
            // Pass in the time you want to start and the interval
            WhatsAppStartTimer(new TimeSpan(24, 0, 0), new TimeSpan(24, 0, 0));
        }

        protected void WhatsAppStartTimer(TimeSpan scheduledRunTime, TimeSpan timeBetweenEachRun)
        {
            // Initialize timer
            double current = DateTime.Now.TimeOfDay.TotalMilliseconds;
            double scheduledTime = scheduledRunTime.TotalMilliseconds;
            double intervalPeriod = timeBetweenEachRun.TotalMilliseconds;
            // calculates the first execution of the method, either its today at the scheduled time or tomorrow (if scheduled time has already occurred today)
            double firstExecution = current > scheduledTime ? intervalPeriod - (current - scheduledTime) : scheduledTime - current;

            // create callback - this is the method that is called on every interval
            TimerCallback callback = new TimerCallback(ProcessSendWhatsappNotification);

            // create timer
            this.whatsappnotificationTimer = new Timer(callback, null, Convert.ToInt32(firstExecution), Convert.ToInt32(intervalPeriod));

        }

        public void ProcessSendWhatsappNotification(object state)
        {
            LogHelper.WriteToFile(string.Format("{0}:{1} {2}", this.ServiceName, "Service is recall at", DateTime.Now));
            FollowupNotificationUpdateModel obj = new FollowupNotificationUpdateModel();
            try
            {
                this.followupNotificationRepository.GetFollowupNotificationModel();

                var data = this.followupNotificationRepository.FollowupNotificationList();

                if (data.Count > 0)
                {
                    LogHelper.WriteToFile(string.Format("{0}:Notification detected: {1}", this.ServiceName, data.Count));
                    foreach (var notificationModel in data)
                    {
                        try
                        {

                            obj.NotificationId = notificationModel.NotificationId;
                            notificationModel.Content = notificationModel.Content.Replace("##Name##", notificationModel.StudentName);
                            
                            LogHelper.WriteToFile(string.Format("Notification Content: {0}", notificationModel.Content));

                            if (notificationModel.NotificationType == 3)
                            {
                                WhatsappModel model = new WhatsappModel();
                                if (string.IsNullOrWhiteSpace(notificationModel.SalesPersonWhatappNumber))
                                {
                                    model.FromWhatappNumber = notificationModel.SalesPersonWhatappNumber;
                                }
                                else
                                {
                                    model.FromWhatappNumber = AppConfigHelper.WhatsappNumber.Split(',').FirstOrDefault();
                                }

                                model.MobileNumber = notificationModel.Mobile;
                                model.Content = notificationModel.Content;
                                followupNotificationRepository.SendMessageAsyc(model);
                                obj.Response = "Message Succesfully.";
                            }

                            obj.Status = 1;
                            this.followupNotificationRepository.UpdateNotificationStatus(obj);
                        }
                        catch (Exception ex1)
                        {
                            LogHelper.WriteToFile(string.Format("Notification Content{0}:{1} ", notificationModel.NotificationId, ex1.Message));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Response = "Sending Fail";
                obj.Status = 2;
                this.followupNotificationRepository.UpdateNotificationStatus(obj);
                throw ex;
            }
            LogHelper.WriteToFile(string.Format("{0}:{1} {2}", this.ServiceName, "Service is stopped at", DateTime.Now));
        }
        public static void WhatsappSMSTest()
        {
            try
            {
                var follownotification = new FollowupNotificationRepository();
                WhatsappModel model = new WhatsappModel();
                model.MobileNumber = "7698585516";
                model.Content = "Ekeeda - Hi ##Name##, it is been ##Duration##! Hope you are exploring our courses. Do tell us about your fav class, positive points & challenges. Click - ##Url##";
                model.Content = model.Content.Replace("##Name##", "Urvesh Joshi");
                var duration = DBHelper.getTimeAgo(DateTime.Now.AddDays(-2));
                model.Content = model.Content.Replace("##Duration##", duration);
                follownotification.SendMessageAsyc(model);
            }
            catch (Exception)
            {

                throw;
            }
        }
 
    }
}
