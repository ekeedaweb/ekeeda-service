﻿
namespace Ekeeda.Windows.Services
{
    partial class FollowupServices
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.projectInstaller1 = new Ekeeda.Windows.Services.ProjectInstaller();
            this.whatsAppServices1 = new Ekeeda.Windows.Services.WhatsAppServices();
            // 
            // whatsAppServices1
            // 
            this.whatsAppServices1.ExitCode = 0;
            this.whatsAppServices1.ServiceName = "WhatsAppServices.QA";
            // 
            // FollowupServices
            // 
            this.ServiceName = "Service1";

        }

        #endregion

        private ProjectInstaller projectInstaller1;
        private WhatsAppServices whatsAppServices1;
    }
}
