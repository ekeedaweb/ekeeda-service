﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ekeeda.Windows.Services.Models
{
    public class FollowupNotificationModel 
    {
        public int NotificationId { get; set; }

        public int UserId { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public int Status { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime OrderDate { get; set; }

        public int FeedBackId { get; set; }

        public int NotificationType { get; set; }

        public int NotificationFor { get; set; }

        public int CategoryTypeId { get; set; }

        public string StudentName { get; set; }

        public string SalesPersonWhatappNumber { get; set; }

    }
}
