﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ekeeda.Windows.Services.Models
{
    public class FollowupNotificationUpdateModel
    {
        public int NotificationId { get; set; }
        public int Status { get; set; }
        public string Response { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
