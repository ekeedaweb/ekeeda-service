﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ekeeda.Windows.Services.Models
{
    public class SMSShortURLResponse
    {
        public long id { get; set; }
        public string status { get; set; }
        public string message { get; set; }
        public string token { get; set; }
        public string txtly { get; set; }
    }
}
