﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ekeeda.Windows.Services.Models
{
    public class WhatsappModel
    {
        public string MessageUrl { get; set; }
        public string MessageTitle { get; set; }
        public string Content { get; set; }
        public string MobileNumber { get; set; }
        public string FromWhatappNumber { get; set; }
    }
}
